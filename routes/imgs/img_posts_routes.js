const express = require('express');
const path = require('path');
const multer = require('multer');
const post_modelo = require('../../models/post/modelo_post');
const fs = require('fs');

const app = express();

//Configuracion
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const postId = req.params.id; // Obtén el ID de la publicación desde la solicitud (debe estar autenticado)
        const postFolder = path.join(__dirname, `../../imgs/posts/${postId}`);
        
        // Crea la carpeta de destino si no existe
        fs.mkdirSync(postFolder, { recursive: true });

        cb(null, postFolder); // Ruta donde guardar los archivos
    },
    filename: (req, file, cb) => {
        const fileExtension = path.extname(file.originalname);
        const timestamp = Date.now(); // Genera un timestamp único en milisegundos

        const newFileName = `file_${timestamp}${fileExtension}`;
    
        console.log('Nuevo nombre de archivo:', newFileName); // Agregar este registro
        
        cb(null, newFileName); // Nombre de archivo personalizado
    }
});

const fileFilter = (req, file, cb) => {
    const allowedFileTypes = ['.png', '.jpg', '.jpeg', '.webp', '.pdf'];

    if (allowedFileTypes.includes(path.extname(file.originalname).toLowerCase())) {
        cb(null, true); // Acepta el archivo
    } else {
        cb(new Error('Tipo de archivo no permitido')); // Rechaza el archivo
    }
};

const upload = multer({ 
    storage, 
    fileFilter, 
    limits: {
        fileSize: 4 * 1024 * 1024, // 4MB de límite de tamaño para todos los archivos
    },
});

app.post('/questionatec/api/v2/subir-archivos/post/:id' , upload.array('archivos'), async (req, res) => {
    const id = req.params.id; // ID de la publicación
    const archivos = req.files; // Archivos subidos

    try {
        // Verifica si la publicación con el ID proporcionado existe en la base de datos
        const publicacion = await post_modelo.findById(id);

        let contenido = publicacion.Contenido;

        if (!publicacion) {
            return res.status(404).json({
                ok: false,
                message: 'Publicación no encontrada.'
            });
        }

        if (archivos && archivos.length > 0) {
            let contenidoActualizado = contenido;
        
            for (const archivo of archivos) {
                const nombreArchivo = archivo.filename;
                const rutaArchivo = `/questionatec/api/v2/imagenes/posts/${id}/${nombreArchivo}`;
        
                // Verificar el tipo de archivo
                if (archivo.mimetype.startsWith('image')) {
                    // Si es una imagen, agregar etiqueta <img>
                    const etiquetaImg = `<img src="${rutaArchivo}" />`;
                    contenidoActualizado += etiquetaImg;
                } else if (archivo.mimetype === 'application/pdf') {
                    // Si es un PDF, agregar enlace <a> con el nombre del archivo y extensión .pdf
                    const nombrePDF = `nombreArchivo`;
                    const enlacePDF = `<a class="pdf-file" href="${rutaArchivo}">${nombrePDF}</a>`;
                    contenidoActualizado += enlacePDF;
                }
            }
        
            // Asignar el contenido actualizado a la publicación
            publicacion.Contenido = contenidoActualizado;
        }
        
        // Guardar la publicación actualizada en la base de datos
        await publicacion.save();

        return res.status(200).json({
            ok: true,
            message: 'Contenido de la publicación actualizado.'
        });
    } catch (error) {
        console.error(error);
        return res.status(500).json({
            ok: false,
            message: 'Error al actualizar el contenido de la publicación.'
        });
    }
});

module.exports = app;