const express = require('express');
const path = require('path');
const multer = require('multer');
const fs = require('fs');

const app = express();

// Configuración
const storage = multer.diskStorage({
  destination: path.join(__dirname, '../../imgs/pruebas/'),
  filename: (req, file, cb) => {
    const fileExtension = path.extname(file.originalname);
    const newFileName = `file_${Date.now()}${fileExtension}`;
    cb(null, newFileName);
  }
});

const fileFilter = (req, file, cb) => {
  const allowedFileTypes = ['.png', '.jpg', '.jpeg', '.webp', '.pdf'];
  if (allowedFileTypes.includes(path.extname(file.originalname).toLowerCase())) {
    cb(null, true);
  } else {
    cb(new Error('Tipo de archivo no permitido'));
  }
};

const upload = multer({ 
  storage, 
  fileFilter, 
  limits: {
    fileSize: 4 * 1024 * 1024,
  },
});

app.post('/questionatec/api/v2/prueba-froala' , upload.single('archivo') , async(req, res) => {
    try {
      if (!req.file) {
        return res.status(400).json({
          ok: false,
          message: 'No se recibió ningún archivo.',
        });
      }
  
      const uploadedFile = req.file;
      const fileUrl = `http://localhost:3900/questionatec/api/v2/imagenes/pruebas/${uploadedFile.filename}`;
  
      return res.status(200).json({
        link: fileUrl,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        ok: false,
        message: 'Error al almacenar el archivo.',
      });
    }
  });
  

module.exports = app;