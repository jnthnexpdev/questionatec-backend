const post_modelo = require('../../models/post/modelo_post');
const usuario_modelo = require('../../models/usuario/modelo_usuario');
const reporte_modelo = require('../../models/reports/reports');

const express = require('express');
const schedule = require('node-schedule');
const axios = require('axios');
const moment = require('moment-timezone');
const zonaHorariaCDMX = 'America/Mexico_City';

// Obtener la fecha y hora actual en la zona horaria de Ciudad de México
const fechaActualCDMX = moment.tz(zonaHorariaCDMX);

const app = express();

const apiUrl = 'http://worldtimeapi.org/api/timezone/America/Mexico_City';

async function obtenerFechaHora() {
    try {
      const response = await axios.get(apiUrl);
      if (response.status === 200) {
        const data = response.data;
        const dateTimeString = data.datetime;
        const dateTime = new Date(dateTimeString);
        const year = dateTime.getFullYear();
        const month = dateTime.getMonth() + 1;
        const day = dateTime.getDate();
        const hours = dateTime.getHours() > 12 ? dateTime.getHours() - 12 : dateTime.getHours();
        const minutes = dateTime.getMinutes() < 10 ? '0' + dateTime.getMinutes() : dateTime.getMinutes();
        const ampm = dateTime.getHours() >= 12 ? 'pm' : 'am';
  
        const fecha = `${day}/${month}/${year}`;
        const hora = `${hours}:${minutes} ${ampm}`;
  
        return { fecha, hora };
      } else {
        throw new Error('No se pudo obtener la fecha y hora.');
      }
    } catch (error) {
      console.error('Error al obtener la fecha y hora:', error.message);
      throw error;
    }
}

async function obtenerFecha() {
    try {
        const response = await axios.get(apiUrl);
        if (response.status === 200) {
            const data = response.data;
            const dateTimeString = data.datetime;
            const dateTimeUTC = new Date(dateTimeString);

            // Ajustar la hora a la zona horaria de la Ciudad de México (UTC-6)
            const timeZoneOffset = -6; // Desplazamiento UTC de la Ciudad de México
            const dateTimeCDMX = new Date(dateTimeUTC);
            dateTimeCDMX.setHours(dateTimeCDMX.getHours() + timeZoneOffset);

            return dateTimeCDMX; // Devuelve la hora ajustada a CDMX
        } else {
            throw new Error('No se pudo obtener la fecha y hora.');
        }
    } catch (error) {
        console.error('Error al obtener la fecha y hora:', error.message);
        throw error;
    }
}

//Generar reporte
app.post('/questionatec/api/v2/generar-reporte/', async(req, res) => {
    let body = req.body;

    const { fecha, hora } = await obtenerFechaHora();

    let reporte = new reporte_modelo({
        Autor : body.Autor,
        Usuario : body.Usuario,
        Publicacion : body.Publicacion,
        Fecha : fecha,
        Hora : hora,
        Motivos : body.Motivos,
        Revision : [],
        Estatus : "Pendiente"
    });

    const usuario = await usuario_modelo.findById(body.Usuario);

    if(usuario){
        await usuario_modelo.findByIdAndUpdate(usuario._id, {
            $inc : {'Numero_Reportes' : 1},
        });
    }else{
        return res.status(404).json({
            ok : false,
            message : 'Usuario reportado no encontrado.'
        });
    }

    reporte.save()
    .then((report) => {
        return res.status(200).json({
            ok : true,
            message : 'Reporte generado.',
            reporte : reporte
        });
    })
    .catch((error) => {
        return res.status(500).json({
            ok : false,
            error : error,
            message : 'Ha ocurrido un problema al generar el reporte.',
        });
    });
});

//Revisar reporte
app.post('/questionatec/api/v2/revisar-reporte/:id', async (req, res) => {
    const id = req.params.id;
    const body = req.body;
    const usuarioId = body.Usuario;

    try {
        const fechaSuspension = await obtenerFecha(); // Obtiene la fecha actual

        // Actualizar el reporte y establecerlo como revisado
        const updatedReport = await reporte_modelo.findByIdAndUpdate(
            id,
            {
                $push: {
                    Revision: {
                        Admin: body.Admin,
                        Fecha: fechaSuspension,
                        Comentarios: body.Comentarios,
                        Suspencion: body.Suspencion,
                        Baneo: body.Baneo,
                    },
                },
                $set: {
                    Estatus: 'Revisado',
                },
            },
            { new: true }
        );

        if (body.Suspencion === true) {
            // Suspender la cuenta del usuario
            await usuario_modelo.findByIdAndUpdate(usuarioId, {
                $set: {
                    'Estado_Cuenta.Tipo': 'Suspendida',
                    'Estado_Cuenta.Fecha_Suspension': fechaSuspension,
                },
                $inc: { 'Estado_Cuenta.Numero_Suspensiones': 1 },
            });

            // Calcular la fecha y hora de reactivación en la zona horaria de Ciudad de México
            const reactivacionFecha = fechaActualCDMX.clone().add(5, 'minutes');

            // Formatear la fecha y hora de reactivación según sea necesario
            const reactivacionFormateada = reactivacionFecha.format('YYYY-MM-DDTHH:mm:ss.SSSZ');
            console.log('Fecha de reactivación en Ciudad de México:', reactivacionFormateada);


            schedule.scheduleJob(reactivacionFecha.toDate(), async () => {
                // Reactivar la cuenta del usuario
                await usuario_modelo.findByIdAndUpdate(usuarioId, {
                    $set: {
                        'Estado_Cuenta.Tipo': 'Activa',
                        'Estado_Cuenta.Fecha_Suspension': null,
                    },
                });
            });
            console.log("Fecha: ", reactivacionFecha);
        }

        return res.status(200).json({
            ok: true,
            message: 'Reporte revisado.',
            data: updatedReport, // Devolver los datos actualizados del reporte si es necesario
        });
    } catch (error) {
        return res.status(500).json({
            ok: false,
            message: 'Error al actualizar el reporte.',
        });
    }
});


//Ver los reportes
app.get('/questionatec/api/v2/ver-reportes', async(req, res) => {
    const page = parseInt(req.query.page) || 1;
    const perPage = 10;
    const filtro = req.query.filtro; 

    try{
        const totalCount = await reporte_modelo.countDocuments({Estatus : "Pendiente"});

        const cantidad_reportes = await reporte_modelo.countDocuments();

        let reportes = "";

        switch(filtro){
            case 'Ascendente' : 
                reportes = await reporte_modelo.find({Estatus : "Pendiente"})
                .sort({ Fecha: 1 }) 
                .skip((page - 1) * perPage) 
                .limit(perPage);
            break;

            case 'Descendente' : 
                reportes = await reporte_modelo.find({Estatus : "Pendiente"})
                .sort({ Fecha: -1 }) 
                .skip((page - 1) * perPage) 
                .limit(perPage);
            break;

            default : 
                reportes = await reporte_modelo.find({Estatus : "Pendiente"})
                .skip((page - 1) * perPage) 
                .limit(perPage);
            break;
        }

        const totalPages = Math.ceil(totalCount / perPage); 

        const pendientes = await reporte_modelo.countDocuments({Estatus : "Pendiente"});
        const revisados = await reporte_modelo.countDocuments({Estatus : "Revisado"});
        return res.status(200).json({
            ok : true, 
            Cantidad : cantidad_reportes,
            Revisados : revisados,
            Pendientes : pendientes,
            Reportes : reportes,
            totalPages,
            currentPage: page
        });

    }catch(error){
        return res.status(500).json({
            ok : false, 
            message : 'Error en el servidor'
        });
    }

});

//Consultar reporte
app.get('/questionatec/api/v2/reporte/:id', async(req, res) => {
    const id = req.params.id;

    const reporte = await reporte_modelo.findById(id);

    if(reporte){
        return res.status(200).json({
            ok : true,
            reporte : reporte
        });
    }else{
        return res.status(400).json({
            ok : false,
            message : 'No se ha encontrado el reporte'
        });
    }

});

module.exports = app;