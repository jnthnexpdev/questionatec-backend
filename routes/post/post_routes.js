const express = require('express');
const post_modelo = require('../../models/post/modelo_post');
const usuario_modelo = require('../../models/usuario/modelo_usuario');
const axios = require('axios');

const app = express();

const apiUrl = 'http://worldtimeapi.org/api/timezone/America/Mexico_City';

async function obtenerFechaHora() {
  try {
    const response = await axios.get(apiUrl);
    if (response.status === 200) {
      const data = response.data;
      const dateTimeString = data.datetime;
      const dateTime = new Date(dateTimeString);
      const year = dateTime.getFullYear();
      const month = dateTime.getMonth() + 1;
      const day = dateTime.getDate();
      const hours = dateTime.getHours() > 12 ? dateTime.getHours() - 12 : dateTime.getHours();
      const minutes = dateTime.getMinutes() < 10 ? '0' + dateTime.getMinutes() : dateTime.getMinutes();
      const ampm = dateTime.getHours() >= 12 ? 'pm' : 'am';

      const fecha = `${day}/${month}/${year}`;
      const hora = `${hours}:${minutes} ${ampm}`;

      return { fecha, hora };
    } else {
      throw new Error('No se pudo obtener la fecha y hora.');
    }
  } catch (error) {
    console.error('Error al obtener la fecha y hora:', error.message);
    throw error;
  }
}

//Crear un post
app.post('/questionatec/api/v2/nuevo-post/', async (req, res) => {
    let body = req.body;

    const { fecha, hora } = await obtenerFechaHora();

    let post = new post_modelo ({
        Autor : body.Autor,
        Categoria : body.Categoria,
        Titulo : body.Titulo,
        Contenido: body.Contenido,
        Fecha : fecha,
        Hora : hora,
        Numero_Respuestas : 0,
        Respuestas : [],
        Numero_Reportes : 0,
    });

    post.save()
    .then((publicacion) => {
        return res.status(200).json({
            ok : true,
            message : 'Post creado.',
            data : post
        });
    })
    .catch((error) => {
        return res.status(500).json({
            ok : false,
            message : 'Error al publicar el post.',
        });
    });
});

// Agregar respuesta y crear notificación
app.post('/questionatec/api/v2/agregar-respuesta/:id', async (req, res) => {
  try {
    const id = req.params.id; // Id de la publicación
    const body = req.body;
    const { hora, fecha } = await obtenerFechaHora(); // Hora y fecha de la respuesta

    // Obtener el ID del autor de la publicación
    const id_autor_publicacion = await post_modelo.findById(id, 'Autor');

     // Agregar la respuesta a la publicación
    const postUpdate = await post_modelo.findByIdAndUpdate(
      id,
      {
        $push: {
          Respuestas: {
            Autor_Respuesta: body.Autor_Respuesta,
            Contenido_Respuesta: body.Contenido_Respuesta,
            Fecha_Respuesta: fecha,
            Hora_Respuesta: hora,
            Likes: {
              Usuarios: [],
              Conteo: 0,
            },
          },
        },
        $inc: { Numero_Respuestas: 1 },
      },
      { new: true }
    );

    if (id_autor_publicacion.Autor.toString() === req.body.Autor_Respuesta.toString()) {
      return res.status(200).json({
        ok: true,
        message: 'Respuesta agregada.'
      });
    }

    // Crear una notificación
    const nuevaNotificacion = {
      Publicacion: id, // Puedes almacenar la referencia a la publicación
      Usuario: req.body.Autor_Respuesta, // Usuario que respondió
      Fecha: fecha,
      Hora: hora,
    };

    // Agregar la notificación al autor de la publicación
    await usuario_modelo.findByIdAndUpdate(
      id_autor_publicacion.Autor, // ID del autor de la publicación
      { $push: { Notificaciones: nuevaNotificacion } }
    );

    return res.status(200).json({
      ok: true,
      message: 'Respuesta agregada y notificación creada.',
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      message: 'Ha ocurrido un error',
      error: error.message,
    });
  }
});


//Like
// Like
app.post('/questionatec/api/v2/like/:id', async (req, res) => {
  const id = req.params.id; // Id de la publicación
  const respuestaId = req.body.Respuesta; // Id de la respuesta
  const usuarioId = req.body.Usuario; // Id del usuario que dio like
  const autor = req.body.Autor; //Id del usuario autor de la respuesta para +1 like.

  try {
    // Primero, verifica si el usuario ya ha dado "Me gusta" previamente
    const post = await post_modelo.findById(id);
    const respuesta = post.Respuestas.find((r) => r._id == respuestaId);

    if (respuesta.Likes.Usuarios.includes(usuarioId)) {
      // El usuario ya ha dado "Me gusta" previamente, responde con un mensaje adecuado.
      return;
    }

    // Si el usuario no ha dado "Me gusta" previamente, procede a agregar el "Me gusta"
    const updatedPost = await post_modelo.findByIdAndUpdate(
      id,
      {
        $push: {
          'Respuestas.$[respuesta].Likes.Usuarios': usuarioId
        },
        $inc: {
          'Respuestas.$[respuesta].Likes.Conteo': 1
        }
      },
      {
        arrayFilters: [
          { 'respuesta._id': respuestaId }
        ],
        new: true // Esto te devuelve el documento actualizado
      }
    );

    const addLike = await usuario_modelo.findOneAndUpdate({_id : autor}, 
      {
        $inc : {Likes : 1}
      }, 
      {new : true}
    );

    res.status(200).json({
      ok: true,
      message: 'Like agregado correctamente',
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error al agregar el like',
      error: error.message
    });
  }
});

//Quitar like
app.post('/questionatec/api/v2/dislike/:id', async (req, res) => {
  const id = req.params.id; // Id de la publicación
  const respuestaId = req.body.Respuesta; // Id de la respuesta
  const usuarioId = req.body.Usuario; // Id del usuario que dio like
  const autor = req.body.Autor; //Id del usuario autor de la respuesta para +1 like.

  try {
    const updatedPost = await post_modelo.findByIdAndUpdate(
      id,
      {
        $pull: {
          'Respuestas.$[respuesta].Likes.Usuarios': usuarioId
        },
        $inc: {
          'Respuestas.$[respuesta].Likes.Conteo': -1
        }
      },
      {
        arrayFilters: [
          { 'respuesta._id': respuestaId }
        ],
        new: true // Esto te devuelve el documento actualizado
      }
    );

    const addLike = await usuario_modelo.findOneAndUpdate({_id : autor}, 
      {
        $inc : {Likes : -1}
      }, 
      {new : true}
    );

    res.status(200).json({
      ok: true,
      message: 'Dislike',
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error',
      error: error.message
    });
  }
});

app.get('/questionatec/api/v2/posts', async (req, res) => {
  const page = parseInt(req.query.page) || 1;
  const perPage = 5; 

  try {
    const totalCount = await post_modelo.countDocuments(); 

    // Calcular el índice de inicio y fin inverso
    let startIndex = totalCount - ((page - 1) * perPage) - 1;
    let endIndex = startIndex - perPage + 1;

    if (startIndex < 0) {
      startIndex = 0;
    }

    if (endIndex < 0) {
      endIndex = 0;
    }
    let limit = perPage;
    const totalPages = Math.ceil(totalCount / perPage);

    // Verificar si es la última página y ajustar el límite si no hay suficientes publicaciones
    if (page === totalPages && totalCount % perPage !== 0) {
      limit = totalCount % perPage;
    }

    const posts = await post_modelo
      .find()
      .skip(endIndex)
      .limit(limit); 

    res.status(200).json({
      ok: true,
      posts,
      totalPages,
      currentPage: page,
    });
  } catch (error) {

    return res.status(500).json({
      ok: false,
      message: 'Error al obtener las publicaciones.',
    });
  }
});



//Postd de un usuario en especifico por object id
app.get('/questionatec/api/v2/mis-posts/:Autor', async(req, res) => {
  const page = parseInt(req.query.page) || 1; 
  const perPage = 5;
  let AutorTema = req.params.Autor;

  try {
    const totalCount = await post_modelo.countDocuments({Autor : AutorTema}); 

    // Calcular el índice de inicio y fin inverso
    let startIndex = totalCount - ((page - 1) * perPage) - 1;
    let endIndex = startIndex - perPage + 1;

    if (startIndex < 0) {
      startIndex = 0;
    }

    if (endIndex < 0) {
      endIndex = 0;
    }
    let limit = perPage;
    const totalPages = Math.ceil(totalCount / perPage);

    // Verificar si es la última página y ajustar el límite si no hay suficientes publicaciones
    if (page === totalPages && totalCount % perPage !== 0) {
      limit = totalCount % perPage;
    }

    const posts = await post_modelo
      .find({Autor : AutorTema})
      .skip(endIndex)
      .limit(limit); 

    res.status(200).json({
      ok : true,
      posts,
      totalPages,
      currentPage: page
    });
  }catch(error){
    return res.status(500).json({
      ok: false,
      message: 'Error al obtener las publicaciones.',
  });
  }

}); 

//Buscar post por una palabra o tema
app.get('/questionatec/api/v2/feed-buscar/:palabra', async (req, res) => {
  const page = parseInt(req.query.page) || 1; 
  const perPage = 5;
  const palabra = req.params.palabra;

  try {
      const totalCount = await post_modelo.countDocuments({
          $or: [
              { Categoria: { $regex: palabra, $options: 'i' } },
              { Titulo: { $regex: palabra, $options: 'i' } },
          ],
      });

      const posts = await post_modelo
          .find({
              $or: [
                  { Categoria: { $regex: palabra, $options: 'i' } },
                  { Titulo: { $regex: palabra, $options: 'i' } },
              ],
          })
          .sort({Fecha : -1})
          .skip((page - 1) * perPage)
          .limit(perPage);
      
      if (posts.length === 0) {
        // Enviar respuesta cuando no se encuentren coincidencias
        return res.status(200).json({
          ok: false,
          message: 'No se han encontrado coincidencias.',
        });
      }

      const totalPages = Math.ceil(totalCount / perPage);

      res.status(200).json({
          ok: true,
          posts,
          totalPages,
          currentPage: page,
      });
  } catch (error) {
      res.status(500).json({
          ok: false,
          mensaje: 'Error en el servidor',
      });
  }
});

//Buscar post por id
app.get('/questionatec/api/v2/post/:id', async(req, res) => {
  const id = req.params.id;

  try{
    const post = await post_modelo.findById(id);

    if(!post){
      return res.status(400).json({
        ok : false,
        message : 'Post no encontrado.'
      });
    }

    res.status(200).json({
      ok : true,
      informacion : post
    });

  }catch (error){
    return res.status(500).json({
      ok : false,
      message : 'Error interno del servidor.'
    });
  }

});

//Informacion sobre publicaciones
app.get('/questionatec/api/v2/publicaciones/', async (req, res) => {
    try {
      const publicaciones = await post_modelo.countDocuments();
  
      const categorias = await post_modelo.aggregate([
        {
          $group: {
            _id: "$Categoria",
            count: { $sum: 1 }
          }
        }
      ]);
  
      const filteredCategorias = categorias.filter(categoria => {
        const palabra = categoria._id.toLowerCase();
        const palabrasSeparadas = palabra.split(" ");
        return palabrasSeparadas.every(palabra => palabra.length > 1) || categoria.count > 1;
      });
  
      const categoriaCount = filteredCategorias.length;
  
      res.status(200).json({
        ok: true,
        publicaciones: publicaciones,
        categorias: categoriaCount
      });
    } catch (error) {
      res.status(500).json({ ok: false, error: error.message });
    }
});



//Obtener los 5 temas mas populares
app.get('/questionatec/api/v2/categorias-populares/', async(req, res) => {
    const temas = await post_modelo.aggregate([
        { $group: { _id: '$Categoria', count: { $sum: 1 }, ids: { $push: '$_id' } } },
        { $sort: { count: -1 } },
        { $limit: 5 }
      ]);
   
    res.status(200).json({ 
        ok : true,
        temas
    }); 
});

//Eliminar post
app.delete('/questionatec/api/v2/eliminar-post/:id', async(req, res) => {
    let id = req.params.id;
    
    try{
      const eliminar = await post_modelo.findByIdAndDelete(id);

      res.status(200).json({
          ok : true,
          message : 'Post eliminado',
      });
    }catch{
      return res.status(500).json({
        ok : false,
        message : 'No se ha podido eliminar'
      });
    }
});

module.exports = app;